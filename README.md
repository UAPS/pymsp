# pymsp

pymsp uses [pybind11](https://github.com/pybind/pybind11) to seamlessly bind the [C++ msp library](https://github.com/christianrauch/msp) to python.

Install this as a python module:
 `python setup.py install`


Requires for building 

Install [MSP](https://gitlab.com/UAPS/msp)

```
  g++
  cmake
  pybind11
  libasio
```
