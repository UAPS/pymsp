
/*
* Created by tyler on 8/8/18.
* Copyright (c) 2018 Unmanned Aerial Propulsion Systems.
*/


#include <iostream>
#include <regex>


#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include "Messages.cpp"
#include "Protocol.cpp"
#include "FlightController.cpp"

PYBIND11_MODULE(pymsp, m) {
    protocol::init(m);
    messages::init(m);
    flightcontroller::init(m);
}
