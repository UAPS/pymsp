
/*
* Created by tyler on 8/8/18.
* Copyright (c) 2018 Unmanned Aerial Propulsion Systems.
*/


#include <pybind11/pybind11.h>
#include <msp/msp_id.hpp>

namespace py = pybind11;

namespace protocol {
    void init(py::module &m) {
        py::enum_<msp::ID>(m, "ID").
                value("MSP_API_VERSION", msp::ID::MSP_API_VERSION).
                value("MSP_FC_VARIANT", msp::ID::MSP_FC_VARIANT).
                value("MSP_FC_VERSION", msp::ID::MSP_FC_VERSION).
                value("MSP_BOARD_INFO", msp::ID::MSP_BOARD_INFO).
                value("MSP_BUILD_INFO", msp::ID::MSP_BUILD_INFO).
                value("MSP_BATTERY_CONFIG", msp::ID::MSP_BATTERY_CONFIG).
                value("MSP_SET_BATTERY_CONFIG", msp::ID::MSP_SET_BATTERY_CONFIG).
                value("MSP_MODE_RANGES", msp::ID::MSP_MODE_RANGES).
                value("MSP_SET_MODE_RANGE", msp::ID::MSP_SET_MODE_RANGE).
                value("MSP_FEATURE", msp::ID::MSP_FEATURE).
                value("MSP_SET_FEATURE", msp::ID::MSP_SET_FEATURE).
                value("MSP_BOARD_ALIGNMENT", msp::ID::MSP_BOARD_ALIGNMENT).
                value("MSP_SET_BOARD_ALIGNMENT", msp::ID::MSP_SET_BOARD_ALIGNMENT).
                value("MSP_AMPERAGE_METER_CONFIG", msp::ID::MSP_AMPERAGE_METER_CONFIG).
                value("MSP_SET_AMPERAGE_METER_CONFIG", msp::ID::MSP_SET_AMPERAGE_METER_CONFIG).
                value("MSP_MIXER", msp::ID::MSP_MIXER).
                value("MSP_SET_MIXER", msp::ID::MSP_SET_MIXER).
                value("MSP_RX_CONFIG", msp::ID::MSP_RX_CONFIG).
                value("MSP_SET_RX_CONFIG", msp::ID::MSP_SET_RX_CONFIG).
                value("MSP_LED_COLORS", msp::ID::MSP_LED_COLORS).
                value("MSP_SET_LED_COLORS", msp::ID::MSP_SET_LED_COLORS).
                value("MSP_LED_STRIP_CONFIG", msp::ID::MSP_LED_STRIP_CONFIG).
                value("MSP_SET_LED_STRIP_CONFIG", msp::ID::MSP_SET_LED_STRIP_CONFIG).
                value("MSP_RSSI_CONFIG", msp::ID::MSP_RSSI_CONFIG).
                value("MSP_SET_RSSI_CONFIG", msp::ID::MSP_SET_RSSI_CONFIG).
                value("MSP_ADJUSTMENT_RANGES", msp::ID::MSP_ADJUSTMENT_RANGES).
                value("MSP_SET_ADJUSTMENT_RANGE", msp::ID::MSP_SET_ADJUSTMENT_RANGE).
                value("MSP_CF_SERIAL_CONFIG", msp::ID::MSP_CF_SERIAL_CONFIG).
                value("MSP_SET_CF_SERIAL_CONFIG", msp::ID::MSP_SET_CF_SERIAL_CONFIG).
                value("MSP_VOLTAGE_METER_CONFIG", msp::ID::MSP_VOLTAGE_METER_CONFIG).
                value("MSP_SET_VOLTAGE_METER_CONFIG", msp::ID::MSP_SET_VOLTAGE_METER_CONFIG).
                value("MSP_SONAR_ALTITUDE", msp::ID::MSP_SONAR_ALTITUDE).
                value("MSP_ARMING_CONFIG", msp::ID::MSP_ARMING_CONFIG).
                value("MSP_SET_ARMING_CONFIG", msp::ID::MSP_SET_ARMING_CONFIG).
                value("MSP_RX_MAP", msp::ID::MSP_RX_MAP).
                value("MSP_SET_RX_MAP", msp::ID::MSP_SET_RX_MAP).
                value("MSP_REBOOT", msp::ID::MSP_REBOOT).
                value("MSP_BF_BUILD_INFO", msp::ID::MSP_BF_BUILD_INFO).
                value("MSP_DATAFLASH_SUMMARY", msp::ID::MSP_DATAFLASH_SUMMARY).
                value("MSP_DATAFLASH_READ", msp::ID::MSP_DATAFLASH_READ).
                value("MSP_DATAFLASH_ERASE", msp::ID::MSP_DATAFLASH_ERASE).
                value("MSP_LOOP_TIME", msp::ID::MSP_LOOP_TIME).
                value("MSP_SET_LOOP_TIME", msp::ID::MSP_SET_LOOP_TIME).
                value("MSP_FAILSAFE_CONFIG", msp::ID::MSP_FAILSAFE_CONFIG).
                value("MSP_SET_FAILSAFE_CONFIG", msp::ID::MSP_SET_FAILSAFE_CONFIG).
                value("MSP_RXFAIL_CONFIG", msp::ID::MSP_RXFAIL_CONFIG).
                value("MSP_SET_RXFAIL_CONFIG", msp::ID::MSP_SET_RXFAIL_CONFIG).
                value("MSP_SDCARD_SUMMARY", msp::ID::MSP_SDCARD_SUMMARY).
                value("MSP_BLACKBOX_CONFIG", msp::ID::MSP_BLACKBOX_CONFIG).
                value("MSP_SET_BLACKBOX_CONFIG", msp::ID::MSP_SET_BLACKBOX_CONFIG).
                value("MSP_TRANSPONDER_CONFIG", msp::ID::MSP_TRANSPONDER_CONFIG).
                value("MSP_SET_TRANSPONDER_CONFIG", msp::ID::MSP_SET_TRANSPONDER_CONFIG).
                value("MSP_OSD_CHAR_WRITE", msp::ID::MSP_OSD_CHAR_WRITE).
                value("MSP_VTX", msp::ID::MSP_VTX).
                value("MSP_OSD_VIDEO_CONFIG", msp::ID::MSP_OSD_VIDEO_CONFIG).
                value("MSP_SET_OSD_VIDEO_CONFIG", msp::ID::MSP_SET_OSD_VIDEO_CONFIG).
                value("MSP_OSD_VIDEO_STATUS", msp::ID::MSP_OSD_VIDEO_STATUS).
                value("MSP_OSD_ELEMENT_SUMMARY", msp::ID::MSP_OSD_ELEMENT_SUMMARY).
                value("MSP_OSD_LAYOUT_CONFIG", msp::ID::MSP_OSD_LAYOUT_CONFIG).
                value("MSP_SET_OSD_LAYOUT_CONFIG", msp::ID::MSP_SET_OSD_LAYOUT_CONFIG).
                value("MSP_3D", msp::ID::MSP_3D).
                value("MSP_RC_DEADBAND", msp::ID::MSP_RC_DEADBAND).
                value("MSP_SENSOR_ALIGNMENT", msp::ID::MSP_SENSOR_ALIGNMENT).
                value("MSP_LED_STRIP_MODECOLOR", msp::ID::MSP_LED_STRIP_MODECOLOR).
                value("MSP_VOLTAGE_METERS", msp::ID::MSP_VOLTAGE_METERS).
                value("MSP_AMPERAGE_METERS", msp::ID::MSP_AMPERAGE_METERS).
                value("MSP_BATTERY_STATE", msp::ID::MSP_BATTERY_STATE).
                value("MSP_PILOT", msp::ID::MSP_PILOT).
                value("MSP_SET_3D", msp::ID::MSP_SET_3D).
                value("MSP_SET_RC_DEADBAND", msp::ID::MSP_SET_RC_DEADBAND).
                value("MSP_SET_RESET_CURR_PID", msp::ID::MSP_SET_RESET_CURR_PID).
                value("MSP_SET_SENSOR_ALIGNMENT", msp::ID::MSP_SET_SENSOR_ALIGNMENT).
                value("MSP_SET_LED_STRIP_MODECOLOR", msp::ID::MSP_SET_LED_STRIP_MODECOLOR).
                value("MSP_SET_PILOT", msp::ID::MSP_SET_PILOT).
                value("MSP_PASSTHROUGH_SERIAL", msp::ID::MSP_PASSTHROUGH_SERIAL).
                value("MSP_UID", msp::ID::MSP_UID).
                value("MSP_GPSSVINFO", msp::ID::MSP_GPSSVINFO).
                value("MSP_SERVO_MIX_RULES", msp::ID::MSP_SERVO_MIX_RULES).
                value("MSP_SET_SERVO_MIX_RULE", msp::ID::MSP_SET_SERVO_MIX_RULE).
                value("MSP_SET_4WAY_IF", msp::ID::MSP_SET_4WAY_IF).
                value("MSP_IDENT", msp::ID::MSP_IDENT).
                value("MSP_STATUS", msp::ID::MSP_STATUS).
                value("MSP_RAW_IMU", msp::ID::MSP_RAW_IMU).
                value("MSP_SERVO", msp::ID::MSP_SERVO).
                value("MSP_MOTOR", msp::ID::MSP_MOTOR).
                value("MSP_RC", msp::ID::MSP_RC).
                value("MSP_RAW_GPS", msp::ID::MSP_RAW_GPS).
                value("MSP_COMP_GPS", msp::ID::MSP_COMP_GPS).
                value("MSP_ATTITUDE", msp::ID::MSP_ATTITUDE).
                value("MSP_ALTITUDE", msp::ID::MSP_ALTITUDE).
                value("MSP_ANALOG", msp::ID::MSP_ANALOG).
                value("MSP_RC_TUNING", msp::ID::MSP_RC_TUNING).
                value("MSP_PID", msp::ID::MSP_PID).
                value("MSP_BOX", msp::ID::MSP_BOX).
                value("MSP_MISC", msp::ID::MSP_MISC).
                value("MSP_MOTOR_PINS", msp::ID::MSP_MOTOR_PINS).
                value("MSP_BOXNAMES", msp::ID::MSP_BOXNAMES).
                value("MSP_PIDNAMES", msp::ID::MSP_PIDNAMES).
                value("MSP_WP", msp::ID::MSP_WP).
                value("MSP_BOXIDS", msp::ID::MSP_BOXIDS).
                value("MSP_SERVO_CONF", msp::ID::MSP_SERVO_CONF).
                value("MSP_NAV_STATUS", msp::ID::MSP_NAV_STATUS).
                value("MSP_NAV_CONFIG", msp::ID::MSP_NAV_CONFIG).
                value("MSP_CELLS", msp::ID::MSP_CELLS).
                value("MSP_SET_RAW_RC", msp::ID::MSP_SET_RAW_RC).
                value("MSP_SET_RAW_GPS", msp::ID::MSP_SET_RAW_GPS).
                value("MSP_SET_PID", msp::ID::MSP_SET_PID).
                value("MSP_SET_BOX", msp::ID::MSP_SET_BOX).
                value("MSP_SET_RC_TUNING", msp::ID::MSP_SET_RC_TUNING).
                value("MSP_ACC_CALIBRATION", msp::ID::MSP_ACC_CALIBRATION).
                value("MSP_MAG_CALIBRATION", msp::ID::MSP_MAG_CALIBRATION).
                value("MSP_SET_MISC", msp::ID::MSP_SET_MISC).
                value("MSP_RESET_CONF", msp::ID::MSP_RESET_CONF).
                value("MSP_SET_WP", msp::ID::MSP_SET_WP).
                value("MSP_SELECT_SETTING", msp::ID::MSP_SELECT_SETTING).
                value("MSP_SET_HEAD", msp::ID::MSP_SET_HEAD).
                value("MSP_SET_SERVO_CONF", msp::ID::MSP_SET_SERVO_CONF).
                value("MSP_SET_MOTOR", msp::ID::MSP_SET_MOTOR).
                value("MSP_SET_NAV_CONFIG", msp::ID::MSP_SET_NAV_CONFIG).
                value("MSP_SET_ACC_TRIM", msp::ID::MSP_SET_ACC_TRIM).
                value("MSP_ACC_TRIM", msp::ID::MSP_ACC_TRIM).
                value("MSP_BIND", msp::ID::MSP_BIND).
                value("MSP_EEPROM_WRITE", msp::ID::MSP_EEPROM_WRITE).
                value("MSP_DEBUGMSG", msp::ID::MSP_DEBUGMSG).
                value("MSP_DEBUG", msp::ID::MSP_DEBUG);
    }
}