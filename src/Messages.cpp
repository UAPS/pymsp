
/*
* Created by tyler on 8/8/18.
* Copyright (c) 2018 Unmanned Aerial Propulsion Systems. All rights reserved.
*/

#include <msp/msp_msg.hpp>
#include <pybind11/pybind11.h>

namespace py = pybind11;

namespace messages {

    class PyMessage : public msp::Message {
    public:
        using msp::Message::Message;

        msp::ID id() const override {
            PYBIND11_OVERLOAD_PURE(msp::ID, msp::Message, id);
        }

    };

    class PyRequest : public msp::Request {
    public:

        using msp::Request::Request;

        void decode(const msp::ByteVector &data) override {
            PYBIND11_OVERLOAD_PURE(void, msp::Request, decode, data);
        }

    };

    class PyResponse : public msp::Response {
    public:

        using msp::Response::Response;

        msp::ByteVector encode() const override {
            PYBIND11_OVERLOAD_PURE(msp::ByteVector, msp::Response, encode);
        }

    };

    static void init(py::module &m) {
        py::class_<msp::Message, PyMessage> message(m, "Message");
        message.def("id", &msp::Message::id);

        py::class_<msp::Request, PyRequest> request(m, "Request", message);
        request.def("decode", &msp::Request::decode);

        py::class_<msp::Response, PyResponse> response(m, "Response", message);
        response.def("encode", &msp::Response::encode);

        py::enum_<msp::msg::MultiType>(m, "MultiType")
                .value("TRI", msp::msg::MultiType::TRI)
                .value("QUADP", msp::msg::MultiType::QUADP)
                .value("QUADX", msp::msg::MultiType::QUADX)
                .value("BI", msp::msg::MultiType::BI)
                .value("GIMBAL", msp::msg::MultiType::GIMBAL)
                .value("Y6", msp::msg::MultiType::Y6)
                .value("HEX6", msp::msg::MultiType::HEX6)
                .value("FLYING_WING", msp::msg::MultiType::FLYING_WING)
                .value("Y4", msp::msg::MultiType::Y4)
                .value("HEX6X", msp::msg::MultiType::HEX6X)
                .value("OCTOX8", msp::msg::MultiType::OCTOX8)
                .value("OCTOFLATP", msp::msg::MultiType::OCTOFLATP)
                .value("OCTOFLATX", msp::msg::MultiType::OCTOFLATX)
                .value("AIRPLANE", msp::msg::MultiType::AIRPLANE)
                .value("HELI_120_CCPM", msp::msg::MultiType::HELI_120_CCPM)
                .value("HELI_90_DEG", msp::msg::MultiType::HELI_90_DEG)
                .value("VTAIL4", msp::msg::MultiType::VTAIL4)
                .value("HEX6H", msp::msg::MultiType::HEX6H)
                .value("DUALCOPTER", msp::msg::MultiType::DUALCOPTER)
                .value("SINGLECOPTER", msp::msg::MultiType::SINGLECOPTER);


        py::enum_<msp::msg::Capability>(m, "Capability")
                .value("BIND", msp::msg::Capability::BIND)
                .value("DYNBAL", msp::msg::Capability::DYNBAL)
                .value("FLAP", msp::msg::Capability::FLAP)
                .value("NAVCAP", msp::msg::Capability::NAVCAP)
                .value("EXTAUX", msp::msg::Capability::EXTAUX);

        py::class_<msp::msg::Ident>(m, "Ident", request)
                .def(py::init<>())
                .def_readonly("version", &msp::msg::Ident::version)
                .def_readonly("msp_version", &msp::msg::Ident::msp_version)
                .def_readonly("multitype", &msp::msg::Ident::type)
                .def_readonly("capabilities", &msp::msg::Ident::capabilities)
                .def("has", &msp::msg::Ident::has)
                .def("hasBind", &msp::msg::Ident::hasBind)
                .def("hasDynBal", &msp::msg::Ident::hasDynBal)
                .def("hasFlap", &msp::msg::Ident::hasFlap);

        py::class_<msp::msg::SetRc>(m, "SetRc", response)
                .def(py::init<>())
                .def_readwrite("channels", &msp::msg::SetRc::channels);

        py::class_<msp::msg::Rc>(m, "Rc", request)
                .def(py::init<>())
                .def_readonly("channels", &msp::msg::Rc::channels);

        py::class_<msp::msg::RcTuning>(m, "RcTuning", request)
                .def(py::init<>())
                .def_readwrite("rc_rate", &msp::msg::RcTuning::RC_RATE)
                .def_readwrite("rc_expo", &msp::msg::RcTuning::RC_EXPO)
                .def_readwrite("roll_pitch_rate", &msp::msg::RcTuning::RollPitchRate)
                .def_readwrite("yaw_rate", &msp::msg::RcTuning::YawRate)
                .def_readwrite("dyn_thr_pid", &msp::msg::RcTuning::DynThrPID)
                .def_readwrite("throttle_mid", &msp::msg::RcTuning::Throttle_MID)
                .def_readwrite("throttle_expo", &msp::msg::RcTuning::Throttle_EXPO);

        py::class_<msp::msg::SetRcTuning>(m, "SetRcTuning", response)
                .def(py::init<msp::msg::RcTuning &>())
                .def_readwrite("rc_rate", &msp::msg::SetRcTuning::RC_RATE)
                .def_readwrite("rc_expo", &msp::msg::SetRcTuning::RC_EXPO)
                .def_readwrite("roll_pitch_rate", &msp::msg::SetRcTuning::RollPitchRate)
                .def_readwrite("yaw_rate", &msp::msg::SetRcTuning::YawRate)
                .def_readwrite("dyn_thr_pid", &msp::msg::SetRcTuning::DynThrPID)
                .def_readwrite("throttle_mid", &msp::msg::SetRcTuning::Throttle_MID)
                .def_readwrite("throttle_expo", &msp::msg::SetRcTuning::Throttle_EXPO);

        py::class_<msp::msg::Analog>(m, "Analog", request)
                .def(py::init<>())
                .def_readonly("vbat", &msp::msg::Analog::vbat)
                .def_readonly("amperage", &msp::msg::Analog::amperage)
                .def_readonly("power_meter_sum", &msp::msg::Analog::powerMeterSum)
                .def_readonly("rssi", &msp::msg::Analog::rssi);

        py::class_<msp::msg::AccCalibration>(m, "AccCalibration", response)
                .def(py::init<>());

        py::class_<msp::msg::MagCalibration>(m, "MagCalibration", response)
                .def(py::init<>());

        py::class_<msp::msg::ResetConfig>(m, "ResetConfig", response)
                .def(py::init<>());

        py::class_<msp::msg::RawGPS>(m, "RawGPS", request)
                .def(py::init<>())
                .def_readonly("fix", &msp::msg::RawGPS::fix)
                .def_readonly("num_sat", &msp::msg::RawGPS::numSat)
                .def_readonly("lat", &msp::msg::RawGPS::lat)
                .def_readonly("lon", &msp::msg::RawGPS::lon)
                .def_readonly("altitude", &msp::msg::RawGPS::altitude)
                .def_readonly("speed", &msp::msg::RawGPS::speed)
                .def_readonly("ground_course", &msp::msg::RawGPS::ground_course);


        py::class_<msp::msg::CompGPS>(m, "CompGPS", request)
                .def(py::init<>())
                .def_readonly("distance_to_home", &msp::msg::CompGPS::distanceToHome)
                .def_readonly("direction_to_home", &msp::msg::CompGPS::directionToHome)
                .def_readonly("update", &msp::msg::CompGPS::update);
    }

}