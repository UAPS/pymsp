
/*
* Created by Tyler Marshall on 8/8/18.
* Copyright (c) 2018 Unmanned Aerial Propulsion Systems. All rights reserved.
*/

#include <msp/FlightController.hpp>


#include <pybind11/pybind11.h>

namespace py = pybind11;

namespace flightcontroller {


    void init(py::module &m) {
        py::enum_<fcu::FirmwareType>(m, "FirmwareType")
                .value("MULTIWII", fcu::FirmwareType::MULTIWII)
                .value("CLEANFLIGHT", fcu::FirmwareType::CLEANFLIGHT);


        py::class_<fcu::FlightController>(m, "FlightController")
                .def(py::init<const std::string &, const size_t>(), py::arg("device"), py::arg("baudrate") = 115200)
                .def("initialise", &fcu::FlightController::initialise)
                .def("is_firmware", &fcu::FlightController::isFirmware)
                .def("is_firmware_multiwii", &fcu::FlightController::isFirmwareMultiWii)
                .def("is_firmware_cleanflight", &fcu::FlightController::isFirmwareCleanflight)
                .def("send_request", &fcu::FlightController::sendRequest)
                .def("request", &fcu::FlightController::request, py::arg("request"), py::arg("timeout") = 0)
                .def("request_raw", &fcu::FlightController::request_raw, py::arg("id"), py::arg("data"),
                     py::arg("timeout") = 0)
                .def("respond", &fcu::FlightController::respond, py::arg("response"), py::arg("wait_ack") = true)
                .def("respond_raw", &fcu::FlightController::respond_raw, py::arg("id"), py::arg("data"),
                     py::arg("wait_ack") = true)
                .def("init_boxes", &fcu::FlightController::initBoxes)
                .def("has_capability", &fcu::FlightController::hasCapability)
                .def("has_bind", &fcu::FlightController::hasBind)
                .def("has_dyn_bal", &fcu::FlightController::hasDynBal)
                .def("has_flap", &fcu::FlightController::hasFlap)
                .def("has_sensor", &fcu::FlightController::hasSensor)
                .def("has_accelerometer", &fcu::FlightController::hasAccelerometer)
                .def("has_barometer", &fcu::FlightController::hasBarometer)
                .def("has_magnetometer", &fcu::FlightController::hasMagnetometer)
                .def("has_gps", &fcu::FlightController::hasGPS)
                .def("has_sonar", &fcu::FlightController::hasSonar)
                .def("isArmed", &fcu::FlightController::isArmed)
                .def("is_status_active", &fcu::FlightController::isStatusActive)
                .def("is_status_failsafe", &fcu::FlightController::isStatusFailsafe)
                .def("arm", &fcu::FlightController::arm)
                .def("arm_block", &fcu::FlightController::arm_block)
                .def("disarm_block", &fcu::FlightController::disarm_block)
                .def("set_rc",
                     (bool (fcu::FlightController::*)(const uint16_t, const uint16_t, const uint16_t, const uint16_t,
                                                      const uint16_t, const uint16_t, const uint16_t, const uint16_t,
                                                      const std::vector<uint16_t>)) &fcu::FlightController::setRc,
                     py::arg("roll"),
                     py::arg("pitch"),
                     py::arg("yaw"),
                     py::arg("throttle"),
                     py::arg("aux1") = 1000,
                     py::arg("aux2") = 1000,
                     py::arg("aux3") = 1000,
                     py::arg("aux4") = 1000,
                     py::arg("auxs") = std::vector<uint16_t>()
                )
                .def("set_motor", &fcu::FlightController::setMotors)
                .def("update_features", &fcu::FlightController::updateFeatures)
                .def("reboot", &fcu::FlightController::reboot)
                .def("write_EEPROM", &fcu::FlightController::writeEEPROM);


    }
}
